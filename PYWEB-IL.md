# Notes for PyWeb-IL


## Venue requirements

* A space meant for giving talks (Podium, screen/projector, etc.)
* Free of charge, of course.
* At least 50 seats.
* Located in Tel-Aviv. If we couldn't find anything better, Ramat Gan and Givatayim could work.
* Could host us regularly, i.e. not just one time.
* An optional advantage: Video recording equipment.

## Sponsoring refreshment

* Pizza or sandwitches or ...
* Fruits?
* Sweets?
* Beer and light drinks.

## Planned dates

Basically once in two months on the first Monday of the month.

* 2024-03-04
* 2024-05-20 (postponed because of holocaust day and memorial day)
* 2024-07-01
* 2024-09-02
* 2024-11-04
