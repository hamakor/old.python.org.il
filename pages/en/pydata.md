---
title: PyData
---

PyData has regular [meetings in Tel Aviv](https://www.meetup.com/pydata-tel-aviv/)
and an [annual conference](https://pydata.org/telaviv2023/).

