---
title: Python in Israel
---

# Python in Israel

## About

This is a community homepage for users of the [Python](http://www.python.org/) programming language in Israel.


## Links

* [PyWeb-IL](/en/pyweb)  has regular meetings See the [PyWeb-IL on Meetup](https://www.meetup.com/PyWeb-IL/).
* [PyData Tel Aviv in Meetup](https://www.meetup.com/pydata-tel-aviv/)
* [PyCon IL](https://pycon.org.il/) - annual Python conerence in Israel.
* [PyData Tel Aviv 2023](https://pydata.org/telaviv2023/).
* [Pyweb-IL mailing list](http://groups.google.com/group/pyweb-il).



* [Older static site](/old/) (some links may be broken).
* [Python for school](/course/) (a course in Hebrew).


