---
title: פייתון בישראל
---

# פייתון בישראל

## אודות

אתר זה משמש את קהילת המשתמשים של שפת התכנות
[פייתון](https://he.wikipedia.org/wiki/%D7%A4%D7%99%D7%99%D7%AA%D7%95%D7%9F)
בישראל. אנו מקיימים פגישות תקופתיות (פתוחות לקהל, בד&quot;כ באזור המרכז), דיונים ב<a class="reference" href="http://hamakor.org.il/cgi-bin/mailman/listinfo/python-il">רשימת דוא&quot;ל</a> ופעילויות נוספות. תוכן עדכני יותר ניתן למצוא ב<a class="reference" href="http://wiki.python.org.il/">אתר הויקי</a> שלנו (<a class="reference" href="http://wiki.python.org.il/How_Can_I_Help">עזרה</a> לשיפור דף זה תתקבל בברכה).</p>

## קישורים

* [האתר הסטטי הישן](/old/) (אנגלית, ייתכנו קישורים שבורים).
* [קורס פייתון](/course/) לבית הספר.
* [רשימת הדיוור](http://hamakor.org.il/cgi-bin/mailman/listinfo/python-il).
* [לוח השנה](http://www.google.com/calendar/render?cid=vh8q4sckau0qj1bup5sd92g1hk%40group.calendar.google.com) לפגישות פייתון כלליות.
* [Pyweb-IL](http://groups.google.com/group/pyweb-il): פורום לתשתיות web פייתוניות.
